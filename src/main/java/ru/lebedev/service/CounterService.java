package ru.lebedev.service;

import ru.lebedev.exceptions.CounterExistsException;
import ru.lebedev.exceptions.CounterNotExistsException;
import ru.lebedev.model.Counter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class CounterService {
    private Map<String, Counter> counters = new ConcurrentHashMap<>();
    private AtomicLong counterId = new AtomicLong(0L);


    public String createCounter(String name) {
        if (name != null) {
            if (counters.containsKey(name)) {
                throw new CounterExistsException("Counter with name " + name + " already exists");
            }
        } else {
            name = "CNT_" + counterId.incrementAndGet();
        }
        Counter cnt = new Counter(name, new AtomicLong(0L));
        counters.put(name, cnt);
        return name;
    }

    public long incrementCounter(String name) {
        if (!counters.containsKey(name)) {
            throw new CounterNotExistsException("There is no counter with name " + name);
        }
        return counters.get(name).getCnt().incrementAndGet();
    }

    public List<Counter> getAllCounters() {
        return new ArrayList<>(counters.values());
    }

    public String deleteCounter(String name) {
        if (!counters.containsKey(name)) {
            throw new CounterNotExistsException("There is no counter with name " + name);
        }
        counters.remove(name);
        return name;
    }

    public Counter getCounter(String name) {
        return counters.get(name);
    }

    public long getSum() {
        long sum = 0;
        for (Counter cnt : counters.values()) {
            sum = sum + cnt.getCnt().get();
        }
        return sum;
    }
}
