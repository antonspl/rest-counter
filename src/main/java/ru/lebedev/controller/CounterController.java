package ru.lebedev.controller;

import org.springframework.web.bind.annotation.*;
import ru.lebedev.model.Counter;
import ru.lebedev.service.CounterService;

import java.util.List;

@RestController
public class CounterController {

    private final CounterService counterService;

    public CounterController(CounterService counterService) {
        this.counterService = counterService;
    }

    @PostMapping("/createCounter")
    public String create(@RequestParam(value = "name", required= false) String name) {
        return counterService.createCounter(name);
    }

    @GetMapping("/getAllCounters")
    public List<Counter> getAllCounters() {
        return counterService.getAllCounters();
    }

    @GetMapping("/increment")
    public long increment(@RequestParam(value = "name") String name) {
        return counterService.incrementCounter(name);
    }

    @DeleteMapping("/delete")
    public String delete(@RequestParam(value = "name") String name) {
        return counterService.deleteCounter(name);
    }

    @GetMapping("/getCounter")
    public Counter getCounter(@RequestParam(value = "name") String name) {
        return counterService.getCounter(name);
    }

    @GetMapping("/getSum")
    public long getSum() {
        return counterService.getSum();
    }
}
