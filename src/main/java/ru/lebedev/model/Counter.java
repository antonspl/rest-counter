package ru.lebedev.model;

import lombok.Value;

import java.util.concurrent.atomic.AtomicLong;

@Value
public class Counter {

    private final String name;

    private final AtomicLong cnt;

}
