package ru.lebedev.exceptions;

public class CounterExistsException extends RuntimeException {
    public CounterExistsException(String message) {
        super(message);
    }
}
