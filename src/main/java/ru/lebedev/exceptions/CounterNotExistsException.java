package ru.lebedev.exceptions;

public class CounterNotExistsException extends RuntimeException {
    public CounterNotExistsException(String message) {
        super(message);
    }
}
