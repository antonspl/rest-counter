package ru.lebedev.service;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@SpringBootTest
class CounterServiceTest {

    @Autowired
    CounterService counterService;

    @Test
    void createCounter() {
        String str = counterService.createCounter("aaa");
        Assertions.assertThat(str).isEqualTo("aaa");
    }

    @Test
    void createCounterConcurrency() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(16);
        List<Callable<String>> callableList = new ArrayList<>(100);
        for (int i = 0; i < 100; i++) {
            callableList.add(() -> counterService.createCounter(null));
        }
        List<Future<String>> futures = executorService.invokeAll(callableList);
        List<String> names = futures.stream().map(stringFuture -> {
            try {
                return stringFuture.get();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }).collect(Collectors.toList());
        System.out.println(names);
        Assertions.assertThat(names).doesNotHaveDuplicates();
    }
}