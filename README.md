Project implements logic required in https://gist.github.com/taiberium/dd4a8af2327e18327a1d7c32a83a923c

To test controller via cli:

Create new Counter with name "COOLCNT": curl --data "name=COOLCNT" localhost:8080/createCounter

Create new Counter with default name: curl -X POST localhost:8080/createCounter

Get list od all Counters: curl localhost:8080/getAllCounters

Get Counter with name "COOLCNT": curl localhost:8080/getCounter?name=COOLCNT

Increment Counter with name "COOLCNT": curl localhost:8080/increment?name=COOLCNT

Get sum of all Counters: curl localhost:8080/getSum

Delete Counter with name "COOLCNT": curl -X DELETE localhost:8080/delete -d "name=COOLCNT"